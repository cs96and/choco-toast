#! /usr/bin/python3
# toast.pyw
# -*- coding: utf-8 -*-

# Copyright (C) 2019 Alan Davies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# pylint: disable=no-name-in-module

"""ChocoToast is a system tray app that monitors for Chocolatey package updates"""

import os
import subprocess
import sys
from datetime import timedelta
from PyQt6.QtCore import Qt, QTimer, pyqtSlot
from PyQt6.QtGui import QIcon
from PyQt6.QtWidgets import QApplication, QMenu, QSystemTrayIcon


class ChocoToastApp(QApplication):
    """ChocoToast main application class"""

    def __init__(self, argv):
        """ChocoToastApp constructor"""
        super().__init__(argv)

        self.setApplicationName("ChocoToast")
        self.setApplicationDisplayName("ChocoToast")

        self.init_ui()

        # Create time to check for updates once per day
        self.timer = ChocoToastApp.create_timer(timedelta(days=1), self.check_for_updates, Qt.TimerType.VeryCoarseTimer)

        # Start a single shot timer to check for updates 30 seconds after startup
        self.single_shot_timer = ChocoToastApp.create_timer(timedelta(seconds=30), self.check_for_updates,
                                                            Qt.TimerType.CoarseTimer, True)

    def init_ui(self):
        """Initialize the UI widgets"""
        script_dir = os.path.dirname(os.path.realpath(__file__))
        self.tray_icon = QSystemTrayIcon(QIcon(f"{script_dir}/ChocoToast.ico"), self)
        self.tray_icon.setToolTip("ChocoToast")

        self.menu = QMenu()
        self.menu.addAction("Check For Updates", lambda: self.check_for_updates(True))
        self.menu.addAction("Exit", self.quit)

        self.tray_icon.setContextMenu(self.menu)

        self.tray_icon.show()

        self.aboutToQuit.connect(self.tray_icon.hide)

    @pyqtSlot()
    @pyqtSlot(bool)
    def check_for_updates(self, is_manual_check=False):
        """Check if there are any chocolatey updates"""

        print(f"check_for_updates(is_manual_check={is_manual_check})")

        startupinfo = subprocess.STARTUPINFO()
        startupinfo.dwFlags |= subprocess.STARTF_USESHOWWINDOW
        startupinfo.wShowWindow = subprocess.SW_HIDE

        result = subprocess.run("choco outdated --limit-output --nocolor --ignore-pinned --no-progress --ignore-unfound".split(),
                                capture_output=True, encoding="utf-8", shell=False, startupinfo=startupinfo)

        if abs(result.returncode) != 1:
            # Prevent the startup single-shot timer from firing once we have done one successful check
            self.single_shot_timer = None

            output = result.stdout.splitlines()
            num_package_updates = len(output)

            if num_package_updates != 0:
                # Extract the first column from the output to get the list of updatable packages
                packages = [line.split("|")[0] for line in output]

                comma = ", "
                self.show_message(f"{num_package_updates} chocolatey updates available: {comma.join(packages)}")

            elif is_manual_check:
                self.show_message("All chocolatey packages are\nup-to-date")
        else:
            self.show_message("Error while checking for updates")

    def show_message(self, message):
        self.tray_icon.showMessage("Choco Toast", message, self.tray_icon.icon())

    def create_timer(interval, slot, timer_type, is_single_shot=False):
        """Helper function to create a QT timer and attach it to a slot"""
        timer = QTimer()
        timer.setSingleShot(is_single_shot)
        timer.setTimerType(timer_type)
        timer.timeout.connect(slot)
        timer.start(int(interval.total_seconds() * 1000))
        return timer


def main():
    APP = ChocoToastApp(sys.argv)
    sys.exit(APP.exec())


if __name__ == "__main__":
    main()
